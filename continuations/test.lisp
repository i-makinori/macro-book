#|
 継続 continuations

- 継続とは､動作中のプログラムを凍結したもので､即ち､計算処理の状態を含んだ一つの関数オブジェクトである｡ (on lisp)

継続は､その計算処理の未来に､何が計算されるのかを示す｡
|#

;;(/ (- x 1) 2)


#|n
`(- x 1)` が評価された時点では,外側の / 式がその値を待っており｡さらにべtの何かが /式の値を待っている
|#


(lambda (val) (/ val 2))

(defun f1 (w)
  (let ((y (f2 w)))
    (if (integerp y) (list 'a y) 'b)))

(defun f2 (x)
  (/ (- x 1) 2))

(lambda (val)
  (let ((y (/ val 2)))
    (if (integerp y) (list 'a y) 'b)))



;;; call with current continuation

(defparameter *cont* nil)
(setq *cont* #'values)

(defmacro =lambda (params &body body)
  `#'(lambda (*cont* ,@params) ,@body))

(defmacro =defun (name params &body body)
  (let ((f (intern (concatenate 'string
                                "=" (symbol-name name)))))
    `(progn
       (defmacro ,name ,params
         `(,',f *cont* ,,@params))
       (defun ,f (*cont* ,@params) ,@body))))

(defmacro =bind (params expr &body body)
  `(let ((*cont* #'(lambda ,params ,@body))) ,expr))

(defmacro =values (&rest retvals)
  `(funcall *cont* ,@retvals))

(defmacro =funcall (fn &rest args)
  `(funcall ,fn *cont* ,@args))

(defmacro =apply (fn &rest args)
  `(apply ,fn *cont* ,@args))

;;; samples

;;(=defun add1 (x) (=values (1+ x)))

;; (add1 2)
;; => (funcall #'(lambda (*cont* n) (=values (1+ n))) *cont* 2)

;; sample of tree

(defparameter tree1 '(a (b (d h)) (c e (f i) g)))
(defparameter tree2 '(1 (2 (3 6 7) 4 5)))


(defun dft (tree)
  (cond ((null tree) nil)
        ((atom tree) (princ tree))
        (t (dft (car tree))
           xo(dft (cdr tree)))))

(defparameter *saved* nil)
;;(setq *saved* nil)

(=defun dft-node (tree)
  (cond ((null tree) (restart!))
        ((atom tree) (=values tree))
        (t (push #'(lambda () (dft-node (cdr tree)))
                 *saved*)
           (dft-node (car tree)))))

(=defun restart! ()
  (if *saved*
      (funcall (pop *saved*))
      (=values 'done)))

(=defun dft2 (tree)
  (setq *saved* nil)
  (=bind (node) (dft-node tree)
    (cond ((eq node 'done) (=values nil))
          (t (princ node)
             (restart!)))))



;; multiple process



