
;; (lambda (val) (/ val 2)))

(define (f1 w)
  (let ((y (f2 w)))
    (if (integer? y) (list 'a y) 'b)))

(define (f2 x)
  (/ (- x 1) 2))

;;

(define frozen '())


(append '(the call/cc returned)
        (list (call-with-current-continuation
               (lambda (cc)
                 (set! frozen cc)
                 'a)))
        (list 'b))

(frozen 'again)

(frozen 'thrice)

(+ 1 (frozen 'sefely))

;;

(define froz1 '())

(define froz2 '())

(define froz- '())

(let ((x 0))
  (call-with-current-continuation
   (lambda (cc)
     (set! froz1 cc)
     (set! froz2 cc)))
  (call-with-current-continuation
   (lambda (cc)
     (set! froz- cc)
     (set! x (- x 1))))
  (set! x (+ 1 x))
  x)

(froz1 '())
(froz2 '())
(froz- '())


;; tree

(define tree1 '(a (b (d h)) (c e (f i) g)))

(define tree2 '(1 (2 (3 6 7) 4 5)))

(define (dft tree)
  (cond ((null? tree) '())
        ((not (pair? tree)) (write tree))
        (else (dft (car tree))
              (dft (cdr tree)))))

(define *saved* '())

(define (dft-node tree)
  (cond ((null? tree) (result))
        ((not (pair? tree)) tree)
        (else (call-with-current-continuation
               (lambda (cc)
                 (set! *saved*
                   (cons (lambda ()
                           (cc (dft-node (cdr tree)))) ;;car
                         *saved*))
                 (dft-node (car tree))))))) ;;cdr

(define (restart)
  (if (null? *saved*)
      'done
      (let ((cont (car *saved*)))
        (set! *saved* (cdr *saved*))
        (cont))))

(define (dft2 tree)
  (set! *saved* '())
  (let ((node (dft-node tree)))
    (cond ((eq? node 'done) '())
          (else (write node)
                (restart)))))


